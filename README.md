### Env:
ruby 2.4.0

### Installing env
`gem install bundler`

`bundle install`

### Running tests
`ruby unit_tests.rb`

### Running app
`ruby app.rb`

* to exit from the app type `q` and press Enter OR press CTRL+D OR press CTRL+C