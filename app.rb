require_relative 'roman_numbers'

while line = gets.gsub("\n", '')
  break if line == 'q' 
  if (line.split('') & RomanNumbers::MAPPING.values).any? && (line.split('') - RomanNumbers::MAPPING.values).empty?
    puts RomanNumbers.to_int(line)
  elsif line.to_i > 0 and line.to_i < 3999
    puts RomanNumbers.to_roman(line.to_i)
  else
    puts "Invalid input"
  end
end

