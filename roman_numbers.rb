class RomanNumbers
  MAPPING = { 1000=>"M", 900=>"CM", 500=>"D", 400=>"CD", 100=>"C", 90=>"XC", 
              50=>"L", 40=>"XL", 10=>"X", 9=>"IX", 5=>"V", 4=>"IV", 1=>"I" }

  INVERTED_MAPPING = MAPPING.invert

  def self.to_roman(num)
    res = ''
    MAPPING.each do |i, r|
      if num >= i
        res << r * (num / i)
      end
      num = num % i 
    end
    res
  end

  def self.to_int(num)
    res = 0
    chars = num.split('')
    chars.each_with_index do |char, i|
      if chars[i+1] && INVERTED_MAPPING[char] < INVERTED_MAPPING[chars[i+1]]
        res -= INVERTED_MAPPING[char]
      else
        res += INVERTED_MAPPING[char]
      end
    end
    res
  end
end

