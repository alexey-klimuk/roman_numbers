require 'test/unit'
require_relative 'roman_numbers'

class TestRomanNumbers < Test::Unit::TestCase
  def test_to_int
    assert_equal(1, RomanNumbers.to_int('I'))
    assert_equal(2, RomanNumbers.to_int('II'))
    assert_equal(10, RomanNumbers.to_int('X'))
    assert_equal(70, RomanNumbers.to_int('LXX'))
    assert_equal(47, RomanNumbers.to_int('XLVII'))
  end

  def test_to_roman
    assert_equal('III', RomanNumbers.to_roman(3))
    assert_equal('VII', RomanNumbers.to_roman(7))
    assert_equal('CCXXXVII', RomanNumbers.to_roman(237))
    assert_equal('CXLV', RomanNumbers.to_roman(145))
    assert_equal('XII', RomanNumbers.to_roman(12))
  end
end

